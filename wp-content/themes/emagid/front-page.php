<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

	<div id="primary" class="content_area">
        
        
        <section class="jumbotron" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/taxi-cab.jpg)">
            <h1>TAXI MEDALLION AUCTIONS</h1>
        </section>
        
        <section class="announce">
            <h2>Please view our upcoming auctions below</h2>
        </section>
        
        <section class="upcoming_auctions">
            <div class="wrapper">
                                <div class="auction_block"> 
                   <a href="/627-auction/">
                   <div class="auction_bg" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/taxi-city.jpg)"></div>
                    <div class="auction_details">
                        <h4><?php the_field('auction_1_title'); ?></h4>
                        <p><?php the_field('auction_1_date'); ?></p>
                    </div>
                    </a>
                </div>
                
                <div class="auction_block"> 
                    <a href="/717-auction/">
                   <div class="auction_bg" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/taxi-city.jpg)"></div>
                    <div class="auction_details">
                        <h4><?php the_field('auction_2_title'); ?></h4>
                        <p><?php the_field('auction_2_date'); ?></p>
                    </div>
                        </a>
                </div>
                

            </div>
        </section>
        

        
        <section class="contact" id="contact_section">
            <h2>Contact</h2>
 <?php echo do_shortcode('[contact-form-7 id="13" title="Contact Form"]'); ?>
            
        </section>
        
        
        
        
        
	</div><!-- #primary -->


<script>
$(".terms_left").click(function(){
        $(".terms_left img").addClass('hue');
    $(".terms_right img").removeClass('hue');
        $(".terms_right").removeClass('clothed');
    $(this).addClass('clothed');

     $(".terms_right_content").hide();
    $(".terms_left_content").show();
});
$(".terms_right").click(function(){
    $(".terms_right img").addClass('hue');
    $(".terms_left img").removeClass('hue');
        $(".terms_left").removeClass('clothed');
    $(this).addClass('clothed');
    $(".terms_left_content").hide();
    $(".terms_right_content").show();
});
</script>
<?php
//get_sidebar();
get_footer();

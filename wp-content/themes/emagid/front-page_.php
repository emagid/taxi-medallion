<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

	<div id="primary" class="content_area">
        
        
        <section class="jumbotron" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/taxi-cab.jpg)">
<!--            <h1><span>Up to</span> 46 NYC TAXI MEDALLIONS</h1>-->
            <h1 style="text-transform:none;font-size:30pt;"><span></span>No Currently Scheduled Auctions - Please Call to Consign</h1>
        </section>
        
<!--
        <section class="announce">
            <h2>Auction Date & Time</h2>
            <h3 id="date">06 / 14 / 2018</h3>
            <p>Registration: 1pm | Auction: 2pm  </p>
        </section>
-->
        
<!--
        <section class="details" id="details_section">
            <div class="top_half">
                <h3><i>up to</i>  139 NYC Taxi Medallions</h3>
                <h4>Currently Seeking Stalking Horse Bids</h4>
            </div>
            <div class="bottom_half">

                
                <a href="<?php the_field('sale_doc'); ?>" target="_blank">
                    <h4>Notice of Sale</h4>
                </a>
                
                <a href="<?php the_field('terms_doc'); ?>" target="_blank">
                    <h4>Terms of Sale</h4>
                </a>
                
                <a href="<?php the_field('bidder_doc'); ?>" target="_blank">
                    <h4>Bidder ID Form</h4>
                </a>
            </div>
            
            <div class ="details_banner" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/taxi-city.jpg)">
            
            </div>
            
            <style>

            </style>
        </section>
-->
        
<!--
        <section class="auction_terms">
            <div class="wrapper">
                <div class="auction_terms_header">
                    <div class="terms_left term_title clothed">
                        <img class="hue"src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_auction.png">
                        <h2>Bidding Details</h2>
                    </div>
                    <div class="terms_right term_title ">
                        <img  src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_info.png">
                        <h2>Asset Summary</h2>
                    </div>
                </div>

                <div class="terms_left_content">
                    <?php the_field('details'); ?>

                </div>
                <div class="terms_right_content" style="display:none;">
                    <?php the_field('summary'); ?>
                </div>
                </div>
        </section>
-->
<!--
        <section class="details">
                    <div class="bottom_half">

                <?php the_field('legal'); ?>
                
            </div>
        </section>
-->
        <section class="contact" id="contact_section">
            <h2>Contact</h2>
 <?php echo do_shortcode('[contact-form-7 id="13" title="Contact Form"]'); ?>
            
        </section>
        
        
        
        
        
	</div><!-- #primary -->


<script>
$(".terms_left").click(function(){
        $(".terms_left img").addClass('hue');
    $(".terms_right img").removeClass('hue');
        $(".terms_right").removeClass('clothed');
    $(this).addClass('clothed');

     $(".terms_right_content").hide();
    $(".terms_left_content").show();
});
$(".terms_right").click(function(){
    $(".terms_right img").addClass('hue');
    $(".terms_left img").removeClass('hue');
        $(".terms_left").removeClass('clothed');
    $(this).addClass('clothed');
    $(".terms_left_content").hide();
    $(".terms_right_content").show();
});
</script>
<?php
//get_sidebar();
get_footer();

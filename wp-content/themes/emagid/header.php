<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_url'); ?>/assets/fonts/fonts.css">
	<?php wp_head(); ?>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<header class="site_header">
        <div class="wrapper_95" id="header_style">
            <div class="site_branding">
                <a href="/">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/site-logo.png">
                </a>
            </div><!-- .site_branding -->

            <nav class="site_navigation">
                <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-1',
                        'menu_id'        => 'primary-menu',
                    ) );
                ?>
            </nav><!-- .site_navigation -->
        </div>
	</header>

	<div class="site_content">

<?php
/**
 * Template Name: Auction 1 Template
 */
get_header(); ?>

	<div id="primary" class="content_area">
        
        
        <section class="jumbotron" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/taxi-cab.jpg)">
<!--            <h1><span>Up to</span> 46 NYC TAXI MEDALLIONS</h1>-->
            <h1><span></span> <?php the_field('banner_title'); ?></h1>
        </section>
        
        <section class="announce">
            <h2>Auction Date & Time</h2>
            <h3 id="date"><?php the_field('date'); ?></h3>
            <p><?php the_field('time'); ?></p>
        </section>
        
        <section class="details" id="details_section">
            <div class="top_half">
                <h3><i></i> <?php the_field('banner_title'); ?></h3>
            </div>

            
            <div class ="details_banner" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/taxi-city.jpg)">
            
            </div>
            
            <style>

            </style>
        </section>
        
        <section class="auction_terms">
            <div class="wrapper">
                <div class="auction_terms_header">
                    <div class="terms_left term_title clothed">
                        <img class="hue"src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_auction.png">
                        <h2>Asset Summary</h2>
                        
                    </div>
                    <div class="terms_right term_title ">
                        <img  src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_info.png">
                        <h2>Bidding Details</h2>
                    </div>
                </div>

                <div class="terms_left_content">
                    <?php the_field('summary'); ?>
                </div>
                <div class="terms_right_content" style="display:none;">
                    <?php the_field('details'); ?>
                    
                </div>
                
            <div class="bottom_half">
                <?php the_field('legal'); ?>
                
                <?php if (get_field('sale_doc') != ''): ?>
                <a href="<?php the_field('sale_doc'); ?>" target="_blank">
                    <h4 class="left">Notice of Sale</h4>
                </a>
                <?php endif; ?>

            </div>
                </div>
            

        </section>
        
        

        
        
	</div><!-- #primary -->


<script>
$(".terms_left").click(function(){
        $(".terms_left img").addClass('hue');
    $(".terms_right img").removeClass('hue');
        $(".terms_right").removeClass('clothed');
    $(this).addClass('clothed');

     $(".terms_right_content").hide();
    $(".terms_left_content").show();
});
$(".terms_right").click(function(){
    $(".terms_right img").addClass('hue');
    $(".terms_left img").removeClass('hue');
        $(".terms_left").removeClass('clothed');
    $(this).addClass('clothed');
    $(".terms_left_content").hide();
    $(".terms_right_content").show();
});
</script>
<?php
//get_sidebar();
get_footer();

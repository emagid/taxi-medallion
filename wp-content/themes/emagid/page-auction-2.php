<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

	<div id="primary" class="content_area">
        
        
        <section class="jumbotron" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/taxi-cab.jpg)">
<!--            <h1><span>Up to</span> 46 NYC TAXI MEDALLIONS</h1>-->
            <h1><span></span> 6 NYC TAXI MEDALLIONS</h1>
        </section>
        
        <section class="announce">
            <h2>Auction Date & Time</h2>
            <h3 id="date">01 / 11 / 2018</h3>
            <p>Registration: 10:30am | Auction: 11am  </p>
        </section>
        
        <section class="details" id="details_section">
            <div class="top_half">
                <h2>Details</h2>
                <h3><i></i> 6 NYC Taxi Medallions</h3>
                <h4>Bidders May Present Bids on One, Multiple or All Medallions</h4>
            </div>
            <div class="bottom_half">
<!--
                <p><strong>Bankruptcy Auction</strong> - United States Bankruptcy Court Eastern District of New York <br> In Re: Hypnotic Taxi, LLC, et al - Case  # 15-43300(CEC) - Jointly Administered</p>
                <p>Gregory Messer, Esq., Chapter 7 Trustee<br>
LaMonica Herbst & Maniscalco, LLP, Attorneys for the Chapter 7 Trustee<br>
Richard B. Maltz, Auctioneer DCA# 1240836<br>
David A. Constantino, Auctioneer DCA# 1424944
</p>
-->
                <?php the_field('legal'); ?>
                
                <a href="<?php the_field('sale_doc'); ?>" target="_blank">
                    <h4 class="left">Notice of Sale</h4>
                </a>
<!--
                <a href="<?php echo get_template_directory_uri(); ?>/assets/docs/Exhibit B-Bidder ID & Offer Form.pdf" target="_blank">
                    <h4 class="right">Bidder ID & Offer Form</h4>
                </a>
-->
            </div>
            
            <div class ="details_banner" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/taxi-city.jpg)">
            
            </div>
            
            <style>

            </style>
        </section>
        
        <section class="auction_terms">
            <div class="wrapper">
                <div class="auction_terms_header">
                    <div class="terms_left term_title clothed">
                        <img class="hue"src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_auction.png">
                        <h2>Bidding Details</h2>
                    </div>
                    <div class="terms_right term_title ">
                        <img  src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_info.png">
                        <h2>Asset Summary</h2>
                    </div>
                </div>

                <div class="terms_left_content">
                    <?php the_field('details'); ?>
<!--
                    <ul>
                        <li><strong>Overview of Sales Process:</strong> This is a two-step bidding process.  The first step is the submission of binding, sealed bids that are due Monday, September 11, 2017.  The top 7 highest or best Bulk Bids and the top 25 highest or best Individual Bids will be invited to bid at the live auction (subject to the Terms & Conditions of Sale approved by the bankruptcy court) on Monday, September 18, 2017.  Please Note: If two equivalent bids are received, the best bidder will be identified as the first bid received.</li>
                        
                        <li><strong>Sealed Bid/Bidder Qualification Deadline:</strong> Monday, September 11, 2017 before 1:00 pm.  Required Bid Documents (Signed Terms and Conditions of Sale & Bidder Identification and Offer Form) and 15% Deposit (cashier’s check) must be included for consideration.  See attached Terms & Conditions of Sale for details or call David Constantino (x205) or Richard Maltz (x202) for additional information.</li>
                        
                        <li><strong>Sealed Bid Delivery Location:</strong> Richard Maltz, Maltz Auctions, Inc., 39 Windsor Place, Central Islip, NY 11722; with copies to LaMonica, Herbst & Maniscalco, LLP, Attn: Jacqulyn S. Loftin, Esq., 3305 Jerusalem Avenue, Suite 201, Wantagh, NY 11793.</li>
                        
                        <li><strong>Auction Date & Time:</strong> Monday, September 18, 2017 at 11:00 am (Registration commences at 9:30 am).  Only Qualified Bidders as defined in the Terms & Conditions of Sale are qualified to participate. 
                        </li>
                        
                        <li><strong>Auction Location:</strong> New York LaGuardia Marriott Hotel, 102-05 Ditmars Boulevard, East Elmhurst, NY 11369.</li>
                        
                        <li><strong>Terms & Conditions of Sale:</strong> Medallions are sold free and clear of all monetary liens.  All prospective bidders must include a cashier’s check in the amount of 15% of the bid amount made payable to “Gregory Messer, Chapter 7 Trustee”.  The Trustee reserves the right to require potentially qualified bidders to bring an additional deposit (cashier’s check) to the live auction to increase their respective deposit to a total of 15% of the highest sealed bid.  Please download the complete Terms & Conditions of Sale. </li>
                        
                        <li><strong>Buyer’s Premium:</strong> A six (6%) percent Buyer’s Premium will be added to the Successful Bidder’s high bid to determine the contract price to be paid by the Successful Bidder.</li>
                        
                        <li><strong>Buyer Broker Participation:</strong> A two (2%) percent commission will be paid to any properly licensed New York City Medallion Broker who registers a successful buyer in accordance with the Buyer Broker guidelines.  Please download the Broker Participation form for details.</li>
                

                    </ul>
-->
                </div>
                <div class="terms_right_content" style="display:none;">
                    <?php the_field('summary'); ?>
<!--
                    <ul>
                        <li> <span class="color_blue">•</span>   Gregory Messer, Esq., was appointed as Chapter 7 Trustee for the Debtors’ jointly administered bankruptcy estates of Hypnotic Taxi LLC et al.  Each Debtor entity owns two or three Medallions for an aggregate of 46 Medallions issued by the New York City Taxi and Limousine Commission (TLC). </li>
                        <li> <span class="color_blue">•</span>
                        The Trustee has 34 Non-Restricted Medallions, 6 Alternative Fuel/Hybrid Medallions and 6 Handicap Medallions.
                        </li>
                        <li> <span class="color_blue">•</span>
                        This is a two-step bidding process: i) the first step is the submission of binding sealed bids; and ii) the top 7 highest or best multiple medallion bids “Bulk Bids” and the top 25 highest or best individual medallion bids “Individual Bids” will be invited to the live auction.
                        </li>
                        <li> <span class="color_blue">•</span>
                        Bidders May Present Bids on One, Multiple or All Medallions
                        </li>
                        <li> <span class="color_blue">•</span>
                        Bid Submission Documents are Attached Under the “Documents” tab on Right Side of this Page 
                        </li>
                        <li> <span class="color_blue">•</span>
                        Please Call Richard Maltz at 516.349.7022 x 202 with Questions or to Discuss Stalking Horse Opportun
                        </li>
                    </ul>  
-->
                </div>
                </div>
        </section>
        
        <section class="contact" id="contact_section">
            <h2>Contact</h2>
 <?php echo do_shortcode('[contact-form-7 id="13" title="Contact Form"]'); ?>
            
        </section>
        
        
        
        
        
	</div><!-- #primary -->


<script>
$(".terms_left").click(function(){
        $(".terms_left img").addClass('hue');
    $(".terms_right img").removeClass('hue');
        $(".terms_right").removeClass('clothed');
    $(this).addClass('clothed');

     $(".terms_right_content").hide();
    $(".terms_left_content").show();
});
$(".terms_right").click(function(){
    $(".terms_right img").addClass('hue');
    $(".terms_left img").removeClass('hue');
        $(".terms_left").removeClass('clothed');
    $(this).addClass('clothed');
    $(".terms_left_content").hide();
    $(".terms_right_content").show();
});
</script>
<?php
//get_sidebar();
get_footer();

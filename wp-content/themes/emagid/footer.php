<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>

	<footer class="site-footer">
        <div class="wrapper">
            <div class="footer_block_left">
                <ul>
                    <li>39 Windsor Place</li>
                    <li>Central Islip, NY 11722</li>
                    <li>ph: 516.349.7022</li>
                    <li>fax: 516.349.0105</li>
                    <li>info@MaltzAuctions.com</li>
                </ul>

            </div>
            <div class="footer_social">
                <p>Connect with us</p>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/social_links.png">
            </div>
            <div class="footer_block_right">
                <p>Copyright 2006-2017 Maltz Auctions All Rights Reserved<br> Powered by eMagid</p>
            </div>
            </div>
	</footer>

<script>    
$("li.menu-item-10 a").click(function() {
    $('html,body').animate({
        scrollTop: $("#details_section").offset().top},
        'slow');
});
    
    $("li.menu-item-11 a").click(function() {
    $('html,body').animate({
        scrollTop: $("#contact_section").offset().top},
        'slow');
});
</script>
<?php wp_footer(); ?>

</body>
</html>
